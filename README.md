# README #

### Overview ###

* This is the repository for the Firia Horizon XG1 eco processor base code
* Version 1.0.0

### Installation ###

* This is intended to run on Windows
* Required to install Silicon Labs Simplicity Studio v4
* Next add gecko_sdk_suite v2.7 to simplicity Studio
* Change your Simplicity workspace location to FiriaSimplicityWorkspace
* In the Simplicity IDE right click on project explorer
* Import the xg1-eco-mgm12p project (browse to the project inside the workspace)
* NOTE: Do not change any project settings and ensure the project name remains the same
* Now you are ready to build

### License ###

* This library can be used freely to support your XG1 gateway projects
* [MIT](https://choosealicense.com/licenses/mit/)

### Contact ###

* For assistance with this library please contact info@firia.com