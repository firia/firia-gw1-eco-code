#include <stdint.h>
#include <string.h>
#include <stdbool.h>

#include "em_device.h"
#include "em_chip.h"
#include "em_gpio.h"
#include "em_emu.h"
#include "em_cmu.h"
#include "spi_driver.h"

#define LED_PORT gpioPortA
#define LED_PIN 4

#define RPI_POWER_PORT gpioPortB
#define RPI_POWER_PIN 13

#define RTC_IRQ1_PORT gpioPortF
#define RTC_IRQ1_PIN 3

#define SPI_FILL_BYTE_1            0x2C
#define SPI_FILL_BYTE_2            0x77
#define SPI_CHECKSUM_BYTE          0x5C

#define SPI_ACK_MSG                0x01
#define SPI_CMD_LED_ON             0x02 // turn on LED attached to eco processor
#define SPI_CMD_LED_OFF            0x03 // turn off LED attached to eco processor
#define SPI_CMD_RPI_SLEEP_INT_WAKE 0x04 // turn off the power on the RPI and resume on interrupt from RTC
#define SPI_CMD_ECO_OFF            0x05 // put eco processor in deepest sleep (only wake option is reset)

// Counts 1 ms time ticks
volatile uint32_t msTicks = 0;

static uint8_t spi_rx_buffer[128];
static uint8_t spi_tx_buffer[128];

static uint8_t cmd_buffer[4];

volatile bool spi_cmd_rx_flag = false;

void lstn_for_spi_cmd(void);
void send_spi_ack(void);

void delay(uint32_t ms_delay)
{
  uint32_t curTicks;
  curTicks = msTicks;
  while ((msTicks - curTicks) < ms_delay) ;
}

void SysTick_Handler(void)
{
  // Increment counter necessary in delay()
  msTicks++;
}

void led_off(void)
{
  GPIO_PinOutClear(LED_PORT, LED_PIN);
}

void led_on(void)
{
  GPIO_PinOutSet(LED_PORT, LED_PIN);
}

void start_ms_ticks(void)
{
  SysTick_Config(CMU_ClockFreqGet(cmuClock_CORE) / 1000);
}

void init_gpio(void)
{
  CMU_ClockEnable(cmuClock_HFPER, true);
  CMU_ClockEnable(cmuClock_GPIO, true);

  GPIO_PinModeSet(LED_PORT, LED_PIN, gpioModePushPull, 0);
  GPIO_PinOutClear(LED_PORT, LED_PIN);

  GPIO_PinModeSet(RPI_POWER_PORT, RPI_POWER_PIN, gpioModePushPull, 0);
  GPIO_PinOutClear(RPI_POWER_PORT, RPI_POWER_PIN);
}

void deep_sleep_rst_wake(void)
{
  EMU_EM2UnBlock(); // unblocks the ability to enter em2/em3/em4
  EMU_EnterEM4(); // come out of this sleep mode through full reset
}

void GPIO_ODD_IRQHandler(void)
{
  // Clear all odd pin interrupt flags
  GPIO_IntClear(0xAAAA);

  // this interrupt just allows the processor to wake from EM3 sleep
}

void sleep_wait_for_interrupt(void)
{
  GPIO_PinModeSet(RTC_IRQ1_PORT, RTC_IRQ1_PIN, gpioModeInputPull, 1);
  GPIO_ExtIntConfig(RTC_IRQ1_PORT, RTC_IRQ1_PIN, RTC_IRQ1_PIN, 0, 1, true);
  NVIC_EnableIRQ(GPIO_ODD_IRQn);

  EMU_EM2UnBlock(); // unblocks the ability to enter em2/em3/em4
  EMU_EnterEM3(true);

  init_gpio();
}

void kill_power_to_rpi(void)
{
  GPIO_PinOutSet(RPI_POWER_PORT, RPI_POWER_PIN);
}

void restore_power_to_rpi(void)
{
  GPIO_PinOutClear(RPI_POWER_PORT, RPI_POWER_PIN);
}

void prepare_for_int_sleep(void)
{
  uint32_t secs_delay = ((uint32_t)cmd_buffer[1] << 8) | (uint32_t)cmd_buffer[2];
  delay(secs_delay * 1000);
  kill_power_to_rpi();
  sleep_wait_for_interrupt();

  // resumes here after sleep
  restore_power_to_rpi();

  // ends with lstn_for_spi_cmd in execute_spi_cmd
}

void execute_spi_cmd(void)
{
  switch(cmd_buffer[0])
  {
  case SPI_CMD_LED_ON:
    led_on();
    break;
  case SPI_CMD_LED_OFF:
    led_off();
    break;
  case SPI_CMD_RPI_SLEEP_INT_WAKE:
    prepare_for_int_sleep(); // resumes here after sleep
    break;
  case SPI_CMD_ECO_OFF:
    deep_sleep_rst_wake(); // will resume from main after full reset
    break;
  default:
    // do nothing if cmd not recognized
    break;
  }

  lstn_for_spi_cmd();
}

void spi_ack_sent(void)
{
  //execute_spi_cmd();
  spi_cmd_rx_flag = true;
}

void send_spi_ack(void)
{
  spi_start_transfer(spi_tx_buffer, spi_rx_buffer, 4, spi_ack_sent);
}

void spi_cmd_received(void)
{
  memcpy(cmd_buffer, spi_rx_buffer, 4);
  send_spi_ack();
}

void lstn_for_spi_cmd(void)
{
  spi_start_transfer(spi_tx_buffer, spi_rx_buffer, 4, spi_cmd_received);
}

void init_core(void)
{
  CMU_HFRCOBandSet(cmuHFRCOFreq_16M0Hz);

  // Initialize the DCDC converter
  EMU_DCDCInit_TypeDef dcdcInit = EMU_DCDCINIT_DEFAULT;
  EMU_DCDCInit(&dcdcInit);
}

void HardFault_Handler(void)
{
  while(1)
  {
    delay(500);
    led_off();
    delay(500);
    led_on();
  }
}

void populate_ack_msg(void)
{
  // this is the expected ack return
  spi_tx_buffer[0] = SPI_ACK_MSG;
  spi_tx_buffer[1] = SPI_FILL_BYTE_1;
  spi_tx_buffer[2] = SPI_FILL_BYTE_2;
  spi_tx_buffer[3] = SPI_CHECKSUM_BYTE;
}

int main(void)
{
  // Chip errata
  CHIP_Init();

  init_core();
  init_gpio();

  spi_init();
  populate_ack_msg();

  lstn_for_spi_cmd();

  start_ms_ticks();

  while(1)
  {
    delay(1);
    if(spi_cmd_rx_flag == true)
    {
      spi_cmd_rx_flag = false;
      execute_spi_cmd();
    }
  }
}
