#include <stdbool.h>

#include "spi_driver.h"
#include "em_core.h"
#include "em_ldma.h"
#include "em_cmu.h"
#include "em_usart.h"

#define SLAVE_RX_DMA_CHANNEL 0
#define SLAVE_TX_DMA_CHANNEL 1

#define SLAVE_DMA_RX_PERIPH_SIGNAL (LDMA_CH_REQSEL_SIGSEL_USART0RXDATAV | LDMA_CH_REQSEL_SOURCESEL_USART0)
#define SLAVE_DMA_TX_PERIPH_SIGNAL (LDMA_CH_REQSEL_SIGSEL_USART0TXBL | LDMA_CH_REQSEL_SOURCESEL_USART0)

#define SLAVE_USART    USART0
#define SLAVE_CMU_CLK  cmuClock_USART0

#define SLAVE_CLK_PORT       gpioPortC
#define SLAVE_CLK_PIN        8
#define SLAVE_CLK_USART_LOC  USART_ROUTELOC0_CLKLOC_LOC11

#define SLAVE_CS_PORT        gpioPortC
#define SLAVE_CS_PIN         9
#define SLAVE_CS_USART_LOC   USART_ROUTELOC0_CSLOC_LOC11

#define SLAVE_MOSI_PORT      gpioPortC
#define SLAVE_MOSI_PIN       6
#define SLAVE_MOSI_USART_LOC USART_ROUTELOC0_TXLOC_LOC11

#define SLAVE_MISO_PORT      gpioPortC
#define SLAVE_MISO_PIN       7
#define SLAVE_MISO_USART_LOC USART_ROUTELOC0_RXLOC_LOC11

typedef struct {
  uint8_t channel;
  uint32_t chnl_mask;
  LDMA_TransferCfg_t cfg;
  LDMA_Descriptor_t desc;
} spi_dma_cfg;

typedef struct {
  spi_xfer_cb xfer_compl_cb;
  CMU_Clock_TypeDef cmu_clk;
  USART_TypeDef *usart;
  spi_dma_cfg *rx_dma;
  spi_dma_cfg *tx_dma;
  bool xfer_compl;
} spi_hdle;

static bool usarts_initialized = false;

static spi_dma_cfg slave_tx_cfg = {
    .channel = SLAVE_TX_DMA_CHANNEL,
    .chnl_mask = 1 << SLAVE_TX_DMA_CHANNEL,
    .cfg = (LDMA_TransferCfg_t)LDMA_TRANSFER_CFG_PERIPHERAL(SLAVE_DMA_TX_PERIPH_SIGNAL)
};

static spi_dma_cfg slave_rx_cfg = {
    .channel = SLAVE_RX_DMA_CHANNEL,
    .chnl_mask = 1 << SLAVE_RX_DMA_CHANNEL,
    .cfg = (LDMA_TransferCfg_t)LDMA_TRANSFER_CFG_PERIPHERAL(SLAVE_DMA_RX_PERIPH_SIGNAL)
};

static spi_hdle slave = {
    .cmu_clk = SLAVE_CMU_CLK,
    .usart = SLAVE_USART,
    .rx_dma = &slave_rx_cfg,
    .tx_dma = &slave_tx_cfg,
    .xfer_compl = false
};

void slave_spi_init_usart(void)
{
  // Configure GPIO mode for Slave Configuration
  GPIO_PinModeSet(SLAVE_CLK_PORT, SLAVE_CLK_PIN, gpioModeInput, 1);      // CLK is input
  GPIO_PinModeSet(SLAVE_CS_PORT, SLAVE_CS_PIN, gpioModeInput, 1);        // CS is input
  GPIO_PinModeSet(SLAVE_MOSI_PORT, SLAVE_MOSI_PIN, gpioModeInput, 1);    // TX (MOSI) is input
  GPIO_PinModeSet(SLAVE_MISO_PORT, SLAVE_MISO_PIN, gpioModePushPull, 1); // RX (MISO) is push pull

  // Start with default config, then modify as necessary
  USART_InitSync_TypeDef config = USART_INITSYNC_DEFAULT;
  config.master    = false;
  config.clockMode = usartClockMode0; // clock idle low, sample on rising/first edge
  config.msbf      = true;            // send MSB first
  config.enable    = usartDisable;    // Make sure to keep USART disabled until it's all set up
  USART_InitSync(SLAVE_USART, &config);

  // Set USART pin locations
  SLAVE_USART->ROUTELOC0 = (SLAVE_CLK_USART_LOC)  | // CLK
                           (SLAVE_CS_USART_LOC)   | // CS
                           (SLAVE_MOSI_USART_LOC) | // TX (MOSI)
                           (SLAVE_MISO_USART_LOC);  // RX (MISO)

  // Enable USART pins
  SLAVE_USART->ROUTEPEN = USART_ROUTEPEN_CLKPEN | USART_ROUTEPEN_CSPEN | USART_ROUTEPEN_TXPEN | USART_ROUTEPEN_RXPEN;
}

void spi_initial_wake_init()
{
  usarts_initialized = true;

  CMU_ClockEnable(slave.cmu_clk, true);
  slave_spi_init_usart();

  LDMA_Init_t ldmaInit = LDMA_INIT_DEFAULT;
  LDMA_Init(&ldmaInit); // Initializing default LDMA settings
}

// LDMA interrupt handler for ESP32 transfers
void LDMA_IRQHandler()
{
  uint32_t flags = LDMA_IntGet();

  if(flags & slave.rx_dma->chnl_mask)
  {
    LDMA_IntClear(slave.rx_dma->chnl_mask);
    slave.xfer_compl = true;
    slave.xfer_compl_cb();
  }

  else if(flags & slave.tx_dma->chnl_mask)
    LDMA_IntClear(slave.tx_dma->chnl_mask);

}

static void start_dma_transfer(spi_hdle *spi, spi_xfer_cb cb)
{
  spi->xfer_compl_cb = cb;
  spi->xfer_compl = false;
  spi->usart->CMD = USART_CMD_CLEARRX | USART_CMD_CLEARTX;

  LDMA_StartTransfer(spi->tx_dma->channel, &spi->tx_dma->cfg, &spi->tx_dma->desc);
  LDMA_StartTransfer(spi->rx_dma->channel, &spi->rx_dma->cfg, &spi->rx_dma->desc);
}

void spi_init(void)
{
  CMU_ClockEnable(cmuClock_HFPER, true);
  CMU_ClockEnable(cmuClock_GPIO, true);
  CMU_ClockEnable(cmuClock_LDMA, true);

  if(!usarts_initialized)
    spi_initial_wake_init();

  CMU_ClockEnable(slave.cmu_clk, true);
  USART_Enable(slave.usart, usartEnable);
}

void spi_deinit(void)
{
  USART_Enable(slave.usart, usartDisable);
  CMU_ClockEnable(slave.cmu_clk, false);
}

void spi_start_transfer(uint8_t *tx_buf, uint8_t *rx_buf, uint32_t len_bytes, spi_xfer_cb cb)
{
  slave.tx_dma->desc = (LDMA_Descriptor_t)LDMA_DESCRIPTOR_SINGLE_M2P_BYTE(tx_buf, &(slave.usart->TXDATA), len_bytes);
  slave.rx_dma->desc = (LDMA_Descriptor_t)LDMA_DESCRIPTOR_SINGLE_P2M_BYTE(&(slave.usart->RXDATA), rx_buf, len_bytes);
  start_dma_transfer(&slave, cb);
}


