#ifndef __SPI_DRIVER_H
#define __SPI_DRIVER_H

#include <stdint.h>

typedef void (*spi_xfer_cb)();

void spi_init(void);
void spi_deinit(void);
void spi_start_transfer(uint8_t *tx_buf, uint8_t *rx_buf, uint32_t len_bytes, spi_xfer_cb cb);

#endif // __SPI_DRIVER_H
